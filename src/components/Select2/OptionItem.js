import React, { useEffect, useRef, useCallback } from 'react'
import cx from 'suit-cx'

import './OptionItem.scss'

const OptionItem = ({ index, option, focus, setFocus, search, onSelect }) => {

    const ref = useRef(null)

    useEffect(() => {
        if(focus) {
            ref.current.focus()
        }
    }, [focus])

    const handleSelect = useCallback((e) => {
        setFocus(index)
        onSelect(option)
    }, [index, setFocus])


    const getHighlightedText = (text, search) => {

        if(!search) {
            return text
        }

        const parts = text.split(new RegExp(`(${search})`, 'gi'));

        return <span> { parts.map((part, i) => 
            <span key={i} style={part.toLowerCase() === search.toLowerCase() ? { fontWeight: '600' } : {} }>
                { part }
            </span>)
        } </span>
    }

    const classes = cx({
        name: 'OptionItem'
    })

    return (
        <li className={ classes() } onClick={ handleSelect } onKeyPress={ handleSelect } tabIndex={ focus ? 0 : -1 } ref={ ref } >
            <div>{ getHighlightedText(option.name, search) }</div>
            <div><small className={ classes('email') }>{ getHighlightedText(option.email, search) }</small></div>
        </li>
    )
}

export default OptionItem