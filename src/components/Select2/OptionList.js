import React from 'react'
import cx from 'suit-cx'
import PropTypes from 'prop-types'

import EmptyList from './EmptyList'
import OptionItem from './OptionItem'
import useRoveFocus from './useRoveFocus'


import './OptionList.scss'

const propTypes = {
    options: PropTypes.array,
    selected: PropTypes.object,
    onClick: PropTypes.func,
}

const defaultProps = {
    options: [],
    selected: null,
    onClick: () => {},
}

const OptionList = ({ options, onClick, isSearching, ...rest }) => {

    const [focus, setFocus] = useRoveFocus(options.length)

    const classes = cx({ name: 'OptionList'})

    if(options && options.length) {
        return (
            <ul className={ classes() }>
                { options && options.map((option, index) => 
                    {
                        return (
                            <OptionItem 
                                key={ index + '_' + option.ssn }
                                index={ index }
                                option={ option }
                                onClick={ () => onClick(option) }
                                focus={ isSearching ? false : focus === index }
                                setFocus={ setFocus } 
                                { ...rest }
                            />
                        )
                    }
                )}
            </ul>
        )
    } else {
        return ( <EmptyList className={ classes('empty') } /> )
    }

    
}

OptionList.propTypes = propTypes
OptionList.defaultProps = defaultProps
export default OptionList