
import React from 'react'
import cx from 'suit-cx'
import PropTypes from 'prop-types'


const propTypes = {
    text: PropTypes.string
}

const defaultProps = {
    text: 'No results!'
}

const EmptyList = ({ text, className }) => {

    const classes = cx({ name: 'EmptyList' }, {}, className )

    return (
        <div className={ classes() }>{ text }</div>
    )
}

EmptyList.propTypes = propTypes
EmptyList.defaultProps = defaultProps

export default EmptyList