import React from 'react'
import cx from 'suit-cx'
import PropTypes from 'prop-types'


const propTypes = {
    placeholderText: PropTypes.string,
    onChange: PropTypes.func,
}

const defaultProps = {
    placeholderText: 'Þekktir viðtakendur'
}

const SelectSearch = ({ placeholderText, onChange, onBlur, setIsSearching, className }) => {

    const handleKeyDown = (e) => {
        if(e.keyCode === 40) {
            setIsSearching(false)
        }
    }

    const classes = cx({
        name: 'SelectSearch',
        modifiers: ['focus']
    }, {}, className)

    return (
        <div className={ classes() }>
            <input className={ classes('input') } type="text" placeholder={ placeholderText } onChange={ onChange } onBlur={ onBlur } onKeyDown={ handleKeyDown } />
        </div>
    )
}

SelectSearch.propTypes = propTypes
SelectSearch.defaultProps = defaultProps
export default SelectSearch