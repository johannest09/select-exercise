import React, { useState, useRef, useEffect } from 'react'
import cx from 'suit-cx'

import SelectSearch from './SelectSearch'
import OptionList from './OptionList'

import useOutsideClickDetectors from './useOutsideClickDetector'

import './Select.scss'

const Select = ({ options, className }) => {

    const [open, setOpen] = useState(false, '')
    const [searchStr, setSearchStr] = useState('')
    const [isSearching, setIsSearching] = useState(true, '')
    const [selectedOption, setSelectedOption] = useState({})

    const toggle = (shouldOpen) => {

        if(shouldOpen) {
            setOpen(true)
        } else {
            setOpen(false)
        }
    }

    const handleSearch = (e) => {

        const search = e.target.value

        if (search) {
            if(!open) {
                toggle(true)
            }
            setIsSearching(true)
        } else {
            toggle()
            setIsSearching(false)
        }

        setSearchStr(search)
    }

    const clearSearch = () => {
        setIsSearching(false)
        setSearchStr('')
        toggle()
    }

    const handleSelect = (option) => {
        console.log("handleSelect: ", option.name)
        setSelectedOption(option)
        clearSearch()
    }

    const handleBlur = () => {
        setIsSearching(false)
    }

    const selectRef = useRef(null)
    const searchRef = useRef(null)

    useEffect(() => {
        if(!(searchStr && isSearching)) {
            searchRef.current.reset()
        }
    })

    const filteredOptions = options.filter(item => item.name.toLowerCase().includes(searchStr))

    const [isOutside, setIsOutside] = useOutsideClickDetectors(selectRef)

    if(isOutside) {
        setIsOutside(false)
        clearSearch()
    }

    const classes = cx({
        name: 'Select',
    }, {}, className)

    return (
        <div className={classes()} ref={ selectRef } >

{/*             { selectedOption && selectedOption.name && <h3 className={ classes('display') }>{ selectedOption.name }</h3> } */}
            
            <form ref={ searchRef } >
                <SelectSearch onChange={ handleSearch } onBlur={ handleBlur } tabIndex="0" setIsSearching={ setIsSearching } />
            </form>
            
            { open && <OptionList options={filteredOptions} onClick={ handleSelect } onSelect={ handleSelect } selected={ selectedOption } search={ searchStr } isSearching={ isSearching } /> }

        </div>
    )

}

export default Select