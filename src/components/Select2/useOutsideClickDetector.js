
import { useEffect, useState } from 'react'

const useOutsideClickDetector = (ref) => {

    const [isOutside, setIsOutside] = useState(false)

    const handleClickOutside = (e) => {

        if(ref.current && !ref.current.contains(e.target)) {
            setIsOutside(true)
        } 
    }

    useEffect(() => {

        document.addEventListener('mousedown', handleClickOutside, false)

        return (() => {
            document.removeEventListener('mousedown', handleClickOutside, false)
        })
    }, [ref])

    return [isOutside, setIsOutside]
}

export default useOutsideClickDetector