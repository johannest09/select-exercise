import React from 'react'
import PropTypes from 'prop-types'
import cx from 'suit-cx'

import './SelectSearch.scss'

/**
 * # SelectSearch
 */
class SelectSearch extends React.Component {

	static propTypes = {
		/**
		 * Fired when the user types in the search box
		 */
		onChange: PropTypes.func,
		/**
		 * Fired when the user uses the arrow keys in the search box
		 */
		onKeyDown: PropTypes.func,
		/**
		 * A placeholder to show in the search field
		 */
		placeholder: PropTypes.string,
		/**
		 * Whether or not to automatically add focus to the search box
		 */
		focusOnSearch: PropTypes.bool,
	}

	static defaultProps = {
		onChange: () => {},
		onKeyDown: () => {},
		placeholder: 'Þekktir viðtakendur',
		focusOnSearch: true,
	}

	handleKeyPress = (e) => {
		const { onKeyDown } = this.props;

		if(e.keyCode === 13 || e.keyCode === 32 || e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 27) {
			e.stopPropagation()
			e.preventDefault()

			onKeyDown(e)

		}
	}

	
	render () {
		const { placeholder, focusOnSearch, className, onChange, ...rest } = this.props

		const classes = cx({
			name: 'SelectSearch',
		}, {}, className)

		return (
			<div className={ classes() }>
				<input 
					className={ classes('input') }
					ref={ input => this.inputEl = input }
					focus={ "focus" } 
					placeholder={ placeholder } 
					onKeyDown={ (e) => this.handleKeyPress(e) }
					onChange={ onChange }
					{ ...rest } />
			</div>
		)
	}

}

export default SelectSearch
