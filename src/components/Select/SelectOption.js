import React from 'react'
import PropTypes from 'prop-types'
import cx from 'suit-cx'

import './SelectOption.scss'

class SelectOption extends React.Component {

    static propTypes = {
        name: PropTypes.string,
        email: PropTypes.string,
        ssn: PropTypes.string,
        onClick: PropTypes.func,
        selected: PropTypes.bool,
        filtered: PropTypes.bool,
        highlight: PropTypes.string,
    }

    static defaultProps = {
        name: null,
        email: null,
        ssn: null,
        onClick: () => {},
        selected: null,
        filtered: false,
        highlight: null,
    }

    handleClick = ( name, ssn ) => {
		const { onClick } = this.props
		onClick({ name, ssn })
    }
    
    getHighlightedText = (text, highlight) => {

        if(!highlight) {
            return <span>{ text }</span>
        }

        const parts = text.split(new RegExp(`(${highlight})`, 'gi'));

        return <span> { parts.map((part, i) => 
            <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' } : {} }>
                { part }
            </span>)
        } </span>;
    }

    render () {
        const { name, email, ssn, selected, highlight, className } = this.props

        const classes = cx({
            name: 'SelectOption',
            states: [ 'selected', 'expanded' ],
        }, { selected }, className)


        return (
            <div className={ classes() }>
                <div className={ classes('item') } onClick={ () => this.handleClick(name, ssn) }>
                    <div className={ classes('label') }>
                        <div className={ classes('name') }>
                            { this.getHighlightedText(name, highlight ) }
                        </div>
                        <div className={ classes('email') }>{ this.getHighlightedText(email, highlight) }</div>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default SelectOption