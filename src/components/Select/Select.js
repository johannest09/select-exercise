import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import cx from 'suit-cx'

import './Select.scss'

import { getSelectedOption, isEqualValue, filterOptions, selectResultItem } from '../../utils'

import SelectSearch from './SelectSearch'
import SelectOption from './SelectOption'

class Select extends React.Component {

    static propTypes = {
        /**
         * Fired when the user selects an option
         */
        onChange: PropTypes.func,
        /**
         * Fired when the select menu closes
         */
        onClose: PropTypes.func,
        /**
         * Fired when the select menu opens
         */
        onOpen: PropTypes.func,
        /**
		 * Fired when the user performs a search
		 */
        onSearch: PropTypes.func,
        /**
         * Fired when the user blurs the search
         */
        onSearchBlur: PropTypes.func,
        /**
		 * Fired when the user selects an item from the search
		 */
        onSearchSelect: PropTypes.func,
        /**
		 * A unique name for the select element
		 */
        name: PropTypes.string,
        /**
		 * A placeholder to show before the user selects an option
		 */
        placeholder: PropTypes.string,
        /**
		 * Whether or not the select menu should be disabled
		 */
        disabled: PropTypes.bool,
		/**
		 * Whether or not a the user can search for items quickly
		 */
        search: PropTypes.bool,
        /**
		 * Whether or not to automatically add focus to the search box
		 */
        focusOnSearch: PropTypes.bool,
        /**
		 * An array of { name: 'Value 1', email: 'jon@jonson.is', ssn: '1312557755' } objects
		 */
        options: PropTypes.array,
        /**
		 * A placeholder to show in the search field
		 */
        searchPlaceholder: PropTypes.string,
        /**
		 * A text to show when there are no search results
		 */
        noResultsText: PropTypes.string,
    }

    static defaultProps = {
        onChange: () => { },
        onClose: null,
        onOpen: null,
        onSearch: null,
        onSearchBlur: null,
        onSearchSelect: null,
        placeholder: null,
        search: false,
        options: [],
        disabled: false,
        focusOnSearch: true,
        searchPlaceholder: 'Þekktir viðtakendur',
        noResultsText: 'Engar niðurstöður',
    }

    state = {
        open: false,
        selectedValue: '',
        filteredOptions: this.props.options,
        selectedIndex: null,
		bottomUp: false,
		highlight: ''
    }

    componentWillReceiveProps ( nextProps ) {
		if ( this.props.defaultValue !== nextProps.defaultValue || this.props.filterOptions !== nextProps.filterOptions ) {
			this.setState({
				selectedValue: nextProps.defaultValue,
				filteredOptions: nextProps.filterOptions,
				bottomUp: false,
			})
		}
    }
    
    /**
     * Handlers
     */

    handleClick = e => {
		const node = ReactDOM.findDOMNode(this.dropdown)

		if ( node && !node.contains(e.target)) {
			this.handleBlur(e)
		}
	}

    handleSelect = ( option ) => {
		if ( this.props.disabled ) return

        const { onChange } = this.props
		const value = option.ssn

		this.setState({
			selectedValue: value,
			open: false,
			filteredOptions: null,
			selectedIndex: null,
		}, () => {
			onChange(getSelectedOption(value, this.props.options))
			this.myFormRef.reset()
		})
	}

    handleToggle = ( e ) => {

        e.preventDefault()

		const { onOpen, onClose } = this.props
		const { open, bottomUp } = this.state

		const shouldBeOpened = !open

		this.setState({
			open: shouldBeOpened,
		})

		if (shouldBeOpened) {
			setTimeout(() => {
				if (!this.dropdownContent) return false

				const position = this.dropdownContent.getBoundingClientRect().bottom
				const isVisible = position <= window.innerHeight

				//Make the select dropdown open up if there's no room for it on the screen
				if (!isVisible && !bottomUp) {
					this.setState({
						bottomUp: !isVisible,
					})
				}
			}, 10)
		}

		document.addEventListener('click', this.handleClick, false)

		if (onOpen && shouldBeOpened) {
			onOpen(e)
		} else if (onClose && !shouldBeOpened) {
			onClose(e)
		}
    }

    handleBlur = (e) => {
		const { onClose } = this.props

		this.setState({
			open: false,
			filteredOptions: null,
			selectedIndex: null,
			bottomUp: false,
		}, () => {
			this.myFormRef.reset()
		})

		document.removeEventListener('click', this.handleClick, false)

		if (onClose) {
			onClose(e)
		}
    }
    
    handleSearch = (e) => {

		if(!e.target.value && this.state.open) {
			this.handleToggle(e)
		}

		if(!this.state.open) {
			this.handleToggle(e)
		}
		

        const filteredOptions = filterOptions(e.target.value, this.props.options )
        
		this.setState({
			filteredOptions: filteredOptions,
			highlight: e.target.value
		})
    }
    
    handleSearchBlur = (e) => {
		const { onSearchBlur } = this.props

		e.preventDefault()
		e.stopPropagation()

		if (onSearchBlur) {
			onSearchBlur(e)
		}
    }
    
    handleKeyDown = (e, search = true) => {
        
		const { options, onSearch, onSearchSelect } = this.props
		const { filteredOptions, open } = this.state

		const visibleOptions = search ? filteredOptions : options
		const keyCode = e.keyCode

		const keyCodes = {
			DOWN: 40,
			ESC: 27,
			ENTER: 13,
			UP: 38,
			SPACE: 32,
		}

		if (!open) { //Using keys when the SelectOption list is not open
			if (keyCode === keyCodes.SPACE || keyCode === keyCodes.ENTER) {
				this.handleToggle(e)
			}
		} else if (keyCode === keyCodes.ESC) {
			this.handleToggle(e)
		} else {
			const selectedIndex = selectResultItem(e, this.state.selectedIndex, visibleOptions, (value) => {
				this.handleSelect(value)

				if (onSearchSelect) {
					onSearchSelect(value)
				}
			})

			if (selectedIndex >= 0) {
				this._makeElemVisible(selectedIndex)
			}

			this.setState({
				selectedIndex: selectedIndex,
			})

			if (onSearch) {
				onSearch(e)
			}
		}
	}
    
    _makeElemVisible = ( selectedIndex ) => {
		const parent = this.optionsContainer

		if (parent) {
			const elem = parent.childNodes && parent.childNodes[selectedIndex]

			if (elem) {
				const elemRect = elem.getBoundingClientRect()
				const parentRect = parent.getBoundingClientRect()

				const parentTop = parent.scrollTop
				const parentBottom = parent.scrollTop + parentRect.height

				const elemTop = elemRect.height * selectedIndex
				const elemBottom = elemTop + elemRect.height
				const elemIsVisible = elemTop >= parentTop && elemBottom <= parentBottom

				if (!elemIsVisible) {
					parent.scrollTop = elemTop
				}
			}
		}
    }
    
    _renderPlaceholder = (selectedOption, placeholder) => {

		const { open } = this.state

        if(open && selectedOption && selectedOption.name) {
            return selectedOption.name
        } else {
            return placeholder
        }
        
    }
    
    renderVisibleOptionList = (optionList, className) => {
		const { filteredOptions, selectedIndex, highlight } = this.state

		return (
			<div className={ className } ref={ t => this.optionsContainer = t }>
				{ optionList.map(( option, i ) =>
					<SelectOption
						filtered={ filteredOptions !== null && filteredOptions.length > 0 }
						key={ 'selectOption-' + i } id={ 'selectOption-' + i } onClick={ this.handleSelect }
						selected={ selectedIndex !== null && isEqualValue(selectedIndex, i) }
						highlight={ highlight }
						{ ...option } />
				) }
			</div>
		)
    }

    renderHiddenOptionList = (optionList, className) => {
		const { filteredOptions } = this.state

		return (
			<div className={ className } aria-hidden="true">
				{ optionList.map(( option, i ) =>
					<SelectOption
						filtered={ filteredOptions !== null && filteredOptions.length > 0 }
						key={ 'hiddenOption-' + i } { ...option } />
				) }
			</div>
		)
    }

    render() {

        const { className, options, disabled, placeholder, focusOnSearch, search, searchPlaceholder, noResultsText, ...rest } = this.props
        const { open, filteredOptions } = this.state

        const selectedOption = getSelectedOption(this.state.selectedValue, options)

        const classes = cx({
            name: 'Select',
            modifiers: ['bottomUp'],
			states: ['open', 'disabled', 'filtered'],
        }, {
            ...this.props,
			...this.state,
			filtered: filteredOptions && filteredOptions.length > 0,
        }, className)

        const optionList = filteredOptions || options

        //console.log("selected item: ", this.state.selectedValue)

        return (
            <div className={classes()} ref={r => this.dropdown = r}>
                {
                    optionList && optionList.length ?
                    this.renderHiddenOptionList(optionList, classes('options', 'options--hidden') ) : ''
                }
				{ 
					<form ref={ (el) => this.myFormRef = el }>
						<SelectSearch
							className={classes('search')}
							onChange={ this.handleSearch }
							onKeyDown={(e) => this.handleKeyDown(e, false)}
							onBlur={(e) => this.handleSearchBlur(e)}
							placeholder={ this._renderPlaceholder(selectedOption, searchPlaceholder)} 
						/>
					</form>
                    
                }
                { open && 
                    <div className={'container'}>
                        <div className={classes('content')} ref={r => this.dropdownContent = r}>
                           
                            { optionList && optionList.length ? 
                               	this.renderVisibleOptionList(optionList, classes('options'))
                                : <div className={classes('noResults')} >{noResultsText}</div>
                            }

                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default Select