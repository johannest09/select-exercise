import { find, filter, cloneDeep } from 'lodash'


const _containsValue = ( value1, value2 ) => {
	let stringValue1 = '',
		stringValue2 = ''

	if (value1) {
		stringValue1 = value1.toString().toLowerCase()
	}

	if (value2) {
		stringValue2 = value2.toString().toLowerCase()
	}

	return stringValue1.includes(stringValue2)
}

export const getSelectedOption = (selected, options) => {
	if (selected === null || options === null || options === undefined) return null

	const clonedOptions = cloneDeep(options)


	const searchPredicate = item => item.ssn === selected

	return find(clonedOptions, item => searchPredicate(item))
}

/**
 * Use to compare if a string or an int has the same inherit value
 */
export const isEqualValue = ( value1, value2 ) => {
	let stringValue1 = '',
		stringValue2 = ''

	if (value1) {
		stringValue1 = value1.toString().toLowerCase()
	}

	if (value2) {
		stringValue2 = value2.toString().toLowerCase()
	}

	return stringValue1 === stringValue2
}

export const filterOptions = (filterValue, options) => {
	const clonedOptions = cloneDeep(options)	

	const searchPredicate = item => _containsValue(item.ssn, filterValue) || _containsValue(item.name, filterValue)
	const filteredOptions = filter(clonedOptions, item => searchPredicate(item))

	return filterValue ? filteredOptions : null
}

export const selectResultItem = (e, selectedIndex, filteredOptions, onSelect) => {
	const { keyCode } = e

	if (!filteredOptions) {
		return null
	}

	e.stopPropagation()
	e.nativeEvent.stopImmediatePropagation()

	const keyCodes = {
		DOWN: 40,
		ENTER: 13,
		UP: 38,
	}

	if ( keyCode === keyCodes.UP || keyCode === keyCodes.DOWN || keyCode === keyCodes.ENTER ) {
		e.preventDefault()

		if ( keyCode === keyCodes.UP) {
			return !selectedIndex ? null : Math.max(0, parseInt(selectedIndex) - 1)
		} else if ( keyCode === keyCodes.DOWN) {
			return selectedIndex === null ? 0 : Math.min(filteredOptions.length - 1, parseInt(selectedIndex) + 1)
		} else if ( keyCode === keyCodes.ENTER ) {
			if (filteredOptions && filteredOptions[selectedIndex]) {
				onSelect(filteredOptions[selectedIndex])
			}
		}
	}

	return null
}