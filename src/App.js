import React from 'react';
import './App.scss';

import Select from './components/Select/Select'
import Select2 from './components/Select2/Select'

const data = [
	{
		"name": "Jón Þór Þórólfsson",
		"email": "jon@fyrirtaekjalausnir.is",
		"ssn": "1758001830"
	},
	{
		"name": "Geir Hannes Björnsson",
		"email": "geir@bankastarfsemi.com",
		"ssn": "2849003620"
	},
	{
		"name": "Geirþrúður Björk Gunnarsdóttir",
		"email": "geirthrudur@opinberstofnun.is",
		"ssn": "1738005027"
	},
	{
		"name": "Haraldur Fjólmundssson",
		"email": "halli@fjarmalastofnun.is",
		"ssn": "2847003900"
	},
	{
		"name": "Birgitta Líf Brjánsdóttir",
		"email": "birgitta@aviato.is",
		"ssn": "1738005027"
	},
	{
		"name": "Gunnleifur Geirfinnsson",
		"email": "gunnig@bigcorpehf.is",
		"ssn": "1502008492"
	},
	{
		"name": "Birgir Ben Brjánsson",
		"email": "biggi@stofnanaeftirlitid.is",
		"ssn": "2758001830"
	}
]

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<h1>Select excercise</h1>
			</header>

			<h2>Select</h2>
			<Select options={ data } />

			<br />

			<h2>Select 2</h2>
			<small className="small">Simplifed with hooks</small>
			<Select2 options={ data } />
		</div>
	);
}

export default App;
